using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeMovement : MonoBehaviour
{
    string lastKey = "W";
    public float moveInterval = 1f;
    void Start()
    {
        InvokeRepeating("MovementRoutine", moveInterval, moveInterval);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            lastKey = "W";
        }
        else if (Input.GetKeyDown(KeyCode.S))
        {
            lastKey = "S";
        }
        else if (Input.GetKeyDown(KeyCode.A))
        {
            lastKey = "A";
        }
        else if (Input.GetKeyDown(KeyCode.D))
        {
            lastKey = "D";
        }
    }

    void MovementRoutine()
    {
        if (transform.position == GameObject.Find("Fruit").transform.position)
        {
            
        }
        switch (lastKey)
        {
            case "W":
                MovementSwitch(transform.position.z, 5f, Vector3.back, Vector3.forward);
                break;
            case "S":
                MovementSwitch(transform.position.z, -5f, Vector3.forward, Vector3.back);
                break;
            case "A":
                MovementSwitch(transform.position.x, -5f, Vector3.right, Vector3.left);
                break;
            case "D":
                MovementSwitch(transform.position.x, 5f, Vector3.left, Vector3.right);
                break;
        }
    }

    void MovementSwitch(float posCoord, float edgeCoord, Vector3 edgeCase, Vector3 normalCase)
    {
        if (posCoord == edgeCoord)
        {
            transform.position += edgeCase * 10;
        }
        else
        {
            transform.position += normalCase;
        }
        }
    
 }