using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FruitMovement : MonoBehaviour
{
    public float moveInterval = 1f;
    int score = 0;
    void Start()
    {
        MoveFruit();
        InvokeRepeating("FruitCheck", moveInterval + 0.001f, moveInterval);
    }

    void Update()
    {
    }

    void MoveFruit()
    {
        int newX = Random.Range(-5, 6);
        int newZ = Random.Range(-5, 6);
        transform.position = new Vector3(newX, 0, newZ);
    }

    void FruitCheck()
    {
        if (transform.position == GameObject.Find("Snake1").transform.position)
        {
            MoveFruit();
            score++;
            Debug.Log(score);
        }
    }
}
